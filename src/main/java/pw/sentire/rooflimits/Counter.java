package pw.sentire.rooflimits;

import java.util.Date;

public class Counter {
  public int bps = 0;
  public long lastChecked = 0;

  public void add(int i) {
    if (this.lastChecked < new Date().getTime()) {
      this.lastChecked = new Date().getTime() + 1000;
      bps = 0;
    }
    bps = bps + i;
  }
}
