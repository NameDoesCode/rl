package pw.sentire.rooflimits;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.configuration.Configuration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityToggleGlideEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerKickEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class MoveLimits implements Listener {
  public final Configuration config;
  public final int bps;
  public final Map<UUID, Counter> players = new HashMap<>();

  public MoveLimits(Configuration config) {
    this.config = config;
    this.bps = config.getInt("max-bps", 0);
  }

  @EventHandler
  public void join(PlayerJoinEvent e) {
    players.put(e.getPlayer().getUniqueId(), new Counter());
  }

  @EventHandler
  public void quit(PlayerQuitEvent e) {
    players.remove(e.getPlayer().getUniqueId());
  }

  @EventHandler
  public void kick(PlayerKickEvent e) {
    players.remove(e.getPlayer().getUniqueId());
  }

  @EventHandler
  public void move(PlayerMoveEvent e) {
    Player p = e.getPlayer();

    if (bps <= 0 || !p.getWorld().getName().equals("world_nether") || p.getLocation().getY() < 128)
      return;
    Location from = e.getFrom();
    Location to = e.getTo();

    int dist =
            Math.abs(
                    Math.abs(to.getBlockX() - from.getBlockX())
                            + Math.abs(to.getBlockZ() - from.getBlockZ()));

    players.get(p.getUniqueId()).add(dist);

    if (players.get(p.getUniqueId()).bps > bps) {
      e.setCancelled(true);
      p.kickPlayer(
              ChatColor.DARK_RED
                      + "You travelled over "
                      + ChatColor.BLUE
                      + bps
                      + ChatColor.DARK_RED
                      + " blocks in a second.");
    }
  }

  @EventHandler
  public void elytra(EntityToggleGlideEvent e) {
    if (!config.getBoolean("anti-elytra", false)) return;
    if (e.getEntity() instanceof Player) {
      Player p = (Player) e.getEntity();
      if (p.getWorld().getName().equals("world_nether") && p.getLocation().getY() > 127) {
        e.setCancelled(true);
      }
    }
  }
}
