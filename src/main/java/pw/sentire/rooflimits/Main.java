package pw.sentire.rooflimits;

import org.bukkit.configuration.Configuration;
import org.bukkit.plugin.java.JavaPlugin;

public final class Main extends JavaPlugin {

  @Override
  public void onEnable() {
    this.saveDefaultConfig();
    Configuration config = this.getConfig();
    this.getServer().getPluginManager().registerEvents(new BlockLimits(config), this);
    this.getServer().getPluginManager().registerEvents(new MoveLimits(config), this);
  }

  @Override
  public void onDisable() {
    // Plugin shutdown logic
  }
}
