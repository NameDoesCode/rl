package pw.sentire.rooflimits;

import org.bukkit.ChatColor;
import org.bukkit.configuration.Configuration;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.player.PlayerBucketEmptyEvent;

public class BlockLimits implements Listener {
  public int height = 128;

  public BlockLimits(Configuration config) {
    height = height + config.getInt("max-height", 128);
  }

  @EventHandler
  public void place(BlockPlaceEvent e) {
    if (e.getBlock().getY() >= height
        && e.getPlayer().getWorld().getName().equals("world_nether")) {
      e.getPlayer()
          .sendMessage(ChatColor.DARK_RED + "The block limit on the nether ceiling is " + height);
      e.setCancelled(true);
    }
  }

  @EventHandler
  public void useBucket(PlayerBucketEmptyEvent e) {
    if (e.getBlock().getY() >= height
        && e.getPlayer().getWorld().getName().equals("world_nether")) {
      e.getPlayer()
          .sendMessage(ChatColor.DARK_RED + "The block limit on the nether ceiling is " + height);
      e.setCancelled(true);
    }
  }
}
